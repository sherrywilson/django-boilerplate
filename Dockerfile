# Pull base image
FROM python:3.7-slim

# Set environment varibles
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Install make & gcc
RUN apt update
RUN apt install make
RUN apt install libpq-dev gcc -y

# Install dos2unix
RUN apt install dos2unix -y

# Add new user
RUN groupadd -r deepuser && useradd -r -g deepuser deepuser

# Set work directory
WORKDIR /app

# Copy Req.txt
COPY requirements.txt /app/

# Install Requirements
RUN pip install -r requirements.txt

# Copy the code
COPY . /app/

# Expose Django Port
EXPOSE 8009

# Copy the entrypoint script
COPY entry.sh /entry.sh
COPY connect.sh /connect.sh

# Making sure the script is in unix format
RUN dos2unix /entry.sh /connect.sh

# Fix script Permissions
RUN chmod +x /entry.sh /connect.sh

# Entrypoint
ENTRYPOINT ["/entry.sh"]
