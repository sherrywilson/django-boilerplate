import ast
import os
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


def get_bool_from_env(name, default_value):
    if name in os.environ:
        value = os.environ[name]
        try:
            return ast.literal_eval(value)
        except ValueError as e:
            raise ValueError("{} is an invalid value for {}".format(value, name)) from e
    return default_value


def get_list(text):
    return [item.strip() for item in text.split(",")]


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get("SECRET_KEY")

ALLOWED_HOSTS = get_list(os.environ.get("ALLOWED_HOSTS", "localhost,127.0.0.1"))

TIME_ZONE = 'Asia/Kolkata'

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

SITE_ID = 1

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = '{{ project_name }}.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')]
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = '{{ project_name }}.wsgi.application'

STATIC_URL = '/static/'
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'staticfiles/'),
)
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, "media")

# Caching
cache = get_bool_from_env('CACHE', False)
if cache is True:
    CACHES = {
        "default": {
            "BACKEND": "django_redis.cache.RedisCache",
            "LOCATION": os.environ.get('REDIS_LOCATION'),
            "OPTIONS": {
                "CLIENT_CLASS": "django_redis.client.DefaultClient",
            }
        }
    }

# Email
email = get_bool_from_env('EMAIL', False)
if email is True:
    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
    EMAIL_HOST = os.environ.get('EMAIL_HOST')
    EMAIL_PORT = os.environ.get('EMAIL_PORT')
    EMAIL_HOST_USER = os.environ.get('EMAIL_HOST_USER')
    EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_HOST_PASSWORD')
    EMAIL_USE_TLS = get_bool_from_env('EMAIL_USE_TLS', True)
    FROM_EMAIL = os.environ.get('FROM_EMAIL')

# Logging
logging = get_bool_from_env('logging', True)
if logging is True:
    LOGGING = {
        "version": 1,
        "disable_existing_loggers": False,
        "root": {"level": "INFO", "handlers": ["console"]},
        "formatters": {
            "verbose": {
                "format": (
                    "%(levelname)s %(name)s %(message)s [PID:%(process)d:%(threadName)s]"
                )
            },
            "simple": {"format": "%(levelname)s %(message)s"},
        },
        "filters": {"require_debug_false": {"()": "django.utils.log.RequireDebugFalse"}},
        "handlers": {
            "mail_admins": {
                "level": "ERROR",
                "filters": ["require_debug_false"],
                "class": "django.utils.log.AdminEmailHandler",
            },
            "console": {
                "level": "DEBUG",
                "class": "logging.StreamHandler",
                "formatter": "verbose",
            },
        },
        "loggers": {
            "django": {
                "handlers": ["console", "mail_admins"],
                "level": "INFO",
                "propagate": True,
            },
            "django.server": {"handlers": ["console"], "level": "INFO", "propagate": True},
            "app": {"handlers": ["console"], "level": "DEBUG", "propagate": True},
        },
    }

# Amazon S3 configuration
do_space = get_bool_from_env('DO_SPACE', False)
if do_space is True:
    AWS_ACCESS_KEY_ID = os.environ.get("AWS_ACCESS_KEY_ID")
    AWS_SECRET_ACCESS_KEY = os.environ.get("AWS_SECRET_ACCESS_KEY")
    AWS_S3_ENDPOINT_URL = os.environ.get("AWS_S3_ENDPOINT_URL", None)
    AWS_S3_CUSTOM_DOMAIN = os.environ.get("AWS_S3_CUSTOM_DOMAIN")
    STATICFILES_STORAGE = 'custom_storages.StaticStorage'
    DEFAULT_FILE_STORAGE = 'custom_storages.MediaStorage'
    STATICFILES_LOCATION = os.environ.get("STATICFILES_LOCATION")
    MEDIAFILES_LOCATION = os.environ.get("MEDIAFILES_LOCATION")
    AWS_STORAGE_BUCKET_NAME = os.environ.get("AWS_STORAGE_BUCKET_NAME")
    AWS_S3_REGION_NAME = os.environ.get("AWS_S3_REGION_NAME")
    AWS_LOCATION = os.environ.get("AWS_LOCATION", "")
    AWS_S3_FILE_OVERWRITE = get_bool_from_env("AWS_S3_FILE_OVERWRITE", False)
    AWS_S3_OBJECT_PARAMETERS = {
        'CacheControl': 'max-age=86400',
    }

# SMS
sms = get_bool_from_env('SMS', True)
if sms is True:
    KALEYRA_KEY = os.environ.get('KALEYRA_KEY')
    KALEYRA_SENDER_ID = os.environ.get('KALEYRA_SENDER_ID')


# Database
db_type = os.environ.get('DB_TYPE', default='SqlLite')
if db_type == 'SqlLite':
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': 'classmateshop.db.sqlite3'
        }
    }
else:
    DATABASES = {
        'default': {
            'ENGINE': os.environ.get('DB_ENGINE'),
            'NAME': os.environ.get('DB_NAME'),
            'USER': os.environ.get('DB_USER', " get_bool_from_env('DB_NAME',"),
            'PASSWORD': os.environ.get('DB_PASSWORD', ""),
            'HOST': os.environ.get('DB_HOST', ""),
            'PORT': os.environ.get('DB_PORT', "5432"),
        }
    }

sentry = get_bool_from_env('SENTRY', True)
if sentry is True:
    sentry_sdk.init(dsn=os.environ.get('SENTRY_DSN'), integrations=[DjangoIntegration()])


# Elastic
elastic = get_bool_from_env('ELASTIC', False)
if elastic is True:
    ELASTICSEARCH_DSL = {
        'default': {
            "hosts": "config('ELASTIC_HOST'):9200"
        },
    }

# Development
dev_env = os.environ.get('DJ_ENV')
if dev_env == 'development':
    DEBUG = True
    INSTALLED_APPS += [
        'debug_toolbar',
        'autofixture',
        'silk'
    ]

    def show_toolbar(request):
        return True

    DEBUG_TOOLBAR_CONFIG = {
        'SHOW_TOOLBAR_CALLBACK': show_toolbar,
    }
    MIDDLEWARE.insert(0, "silk.middleware.SilkyMiddleware")
    MIDDLEWARE += ['debug_toolbar.middleware.DebugToolbarMiddleware', ]
    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

celery = get_bool_from_env('CELERY', False)
if celery is True:
    CELERY_BROKER_URL = os.environ.get('REDIS_LOCATION')
    CELERY_RESULT_BACKEND = 'django-db'
    CELERY_ACCEPT_CONTENT = ['application/json']
    CELERY_TASK_SERIALIZER = 'json'
    CELERY_RESULT_SERIALIZER = 'json'
    CELERY_TIMEZONE = TIME_ZONE
