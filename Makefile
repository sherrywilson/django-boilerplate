clean: clean_pyc
clean_pyc:
	find . -type f -name "*.pyc" -delete || true

migrate:
	python3 /app/manage.py migrate --noinput

migrations:
	python3 /app/manage.py makemigrations

run:
	python3 /app/manage.py runserver_plus 0.0.0.0:8000

shell:
	python3 /app/manage.py shell

static:
    python3 /app/manage.py collectstatic --no-input --clear